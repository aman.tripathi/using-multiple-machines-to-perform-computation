
from multiprocessing import Process, Pool, Queue, current_process, Manager
from functools import reduce
import queue
import time
from random import randint

'''
This method calculates the sum of numbers by taking the tasks from input queue
and putting the results in the output queue
'''

def doSum(ipQ, opQ):
    while True:
        try:
            inputList = ipQ.get_nowait()
        except queue.Empty:
            break
        else:
            sumNumbers = reduce(lambda x, y: x + y, inputList, 0)
            resultTuple = (sumNumbers,
                           len(inputList),
                           "Sum is: "
                           + str(sumNumbers)
                           + " done by process: "
                           + current_process().name)
            opQ.put(resultTuple)
             
    return True

'''
Generate test data and divided chunks into number of processes available
'''

def createInput(list_size, num_process, ipQ, max_int):
    inputList =[randint(0, max_int) for i in range(list_size)]
    chunkSize = int(len(inputList)/num_process)
    

    start = 0
    end = chunkSize
    for i in range(1, num_process):
        ipQ.put(inputList[start:end])
        start = end
        end = chunkSize * (i+1)
    ipQ.put(inputList[start:])



def main():
    number_of_processes = 4
    max_int = 100
    m = Manager()
    ipQ = m.Queue()
    opQ = m.Queue()
    createInput(1000000, number_of_processes, ipQ, max_int) #total no of integral values to be averaged = 1000000

    with Pool(number_of_processes) as pool:
        r =  [r.get() for r in [pool.apply_async(doSum, [ipQ, opQ]) for x in range(number_of_processes)]] 

    total = 0
    countTotal = 0
    result = []
    for i in range(number_of_processes):
        result.append(opQ.get())
        
    for i in result:
        print(i[2])
        total = i[0] + total
        countTotal = countTotal + i[1]
    if (countTotal != 0):
        print("Avg is: ", total/countTotal)
    else:
        print(0)
        
    return True


if __name__ == "__main__":
    main()
    
