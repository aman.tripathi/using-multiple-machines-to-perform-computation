Function to compute Median and Average with Big Data.

Input - Large file of integers.
Output - Return the exact median and average.

Scenario - There is a huge unsorted log file of Integers or Floats.
This file is too big to be consumed by a single machine. You get 4 machines to consume those log files.
These machines cant share memory but can communicate by normal netowrk IO. When the stream is finished, results
returned should contain the exact median and average of the stream.
